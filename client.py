import requests
import json

# Define the url
url = "http://localhost:8000/chat.chain/run"

# Read the contents of the file
with open("examplemessages.txt", "r") as file:
    messages = file.read()

data = {"messages": messages}

data_json = json.dumps(data)

headers = {"Content-type": "application/json"}

# Post the data to the specified url
response = requests.post(url, data_json, headers=headers)

# Print the status and the response text
# print(f"Status code: {response.status_code}")
# print(f"Response text: {response.text}")

splitted = response.json()["output"].split("\n")

print(splitted)
