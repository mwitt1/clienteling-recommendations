from langchain import LLMChain, PromptTemplate
from langchain.chat_models import ChatOpenAI

# from langchain.llms.huggingface_endpoint import HuggingFaceEndpoint

chat = ChatOpenAI(model="gpt-4", temperature=0.0)  # type: ignore
# llama = HuggingFaceEndpoint(
#     endpoint_url="https://i4jlzkpml7l06nid.us-east-1.aws.endpoints.huggingface.cloud",
#     huggingfacehub_api_token="",
#     task="text-generation",
#     verbose=True,
# )


prompt_template = """You are an assistant program in a clienteling app that provides shortut recommendations for messages to customers.
Please recommend 5 messages that would make sense to send next.
Output these recommendations divided by line breaks. 
ONLY output the messages, and no numbers.

Example:

Hi, congratulations on your purchase! Let me know if you need any assistance.
I am glad to help. If you have any other inquiries, don't hesitate to contact me.
Great that I could be of assistance. Feel free to reach out for any additional consultation.


Chat history: 

{messages}

These are five Chat recommendations:"""


chain = LLMChain(
    llm=chat,
    prompt=PromptTemplate.from_template(prompt_template),
)

if __name__ == "__main__":
    with open("examplemessages.txt", "r") as f:
        messages = f.read()
        print(chain.run({"messages": messages}))
